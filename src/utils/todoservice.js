import axios from 'axios'
//const baseURL = ''

const getAll = () => {
    const request = axios.get('http://localhost:3001/list')
    const fakeResponse = [
        {
            id: 1,
            title: 'Set up MondoDB',
            status: false
        },
        {
            id: 2,
            title: 'Open VSCode',
            status: true
        }
    ]
    
    console.log("connecting to db")

    return request.then(response => response.data).catch(error => fakeResponse)
}

export default { getAll }