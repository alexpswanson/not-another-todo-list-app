import React from 'react'
import './todoItem.css'
import ToDoForm from './todoForm'

function todoItem (props) {

    function toggleEdit(newTitle) {
        console.log('clicked item: '+ props.id + ' ' + newTitle)

    }

    return (
        <tr><td class="todo-item" onClick={toggleEdit}>
            <div>
                <input
                    type="checkbox"
                    defaultChecked={props.status}
                    onChange={() => props.toggleItemCompleted(props.id)}
                />
                <label className={props.status ? 'completed' : 'unfinished'}>{props.title}</label>
                <div>
                    <ToDoForm
                        defaultText={props.title}
                        addItem={toggleEdit}
                    />
                </div>
            </div>
        </td></tr>
    )
}

export default todoItem