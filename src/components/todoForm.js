import React, { useState, useEffect } from 'react'
import './todoForm.css'

function ToDoForm(props) {

    const [name, setName] = useState(props.defaultText)

    function handleChange(e){
        console.log(e.target.value)
        setName(e.target.value)
    }

    function handleSubmit(e) {
        e.preventDefault()
        console.log('todoForm -> Submit')
        console.log(name)
        props.addItem(name)
        setName('')
    }

    return(
        <form onSubmit={handleSubmit}>
            <input type="text" id="new-todo-input" name="text" autoComplete="off" value={name} onChange={handleChange}/>
            <br></br>
            <button class="new-todo-submit" type="submit">Submit</button>
        </form>
    )
}

export default ToDoForm;