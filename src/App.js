import React, { useState, useEffect } from 'react'
import {nanoid} from "nanoid"
import ToDoItem from './components/todoItem'
import ToDoForm from './components/todoForm'
import ToDoService from './utils/todoservice'
import './App.css'

function App() {

    const [todoItems, setToDoItems] = useState([])

    useEffect(() => {
        ToDoService
        .getAll()
        .then(initialItems => {
            setToDoItems(initialItems)
        })
    }, [])

    function addItem(item){
        const newItem = {
            id: "todo-"+nanoid(),
            title: item,
            status: false
        }
        console.log(item)

        setToDoItems([...todoItems, newItem])
    }

    function toggleItemCompleted(id){
        console.log(id)
        const updatedItems = todoItems.map(item => {
            if(id === item.id){
                return{...item, status: !item.status}
            }
            return item
        })

        setToDoItems(updatedItems)
    }

    function updateItem(id, newTitle){
        const editedItems = todoItems.map(item => {
            if(id === item.id){
                return{...item, title: newTitle}
            }
            return item
        })

        setToDoItems(editedItems)
    }

    return (
        <div class="app-wrapper">
            <div class="inner-wrapper">
                <h1 class="app-title">Not Another To Do List App</h1>

                <div class="todo-input-container">
                    <ToDoForm addItem={addItem}/>
                </div>

                <table class="todo-container">
                    {todoItems.map(item => 
                    <ToDoItem
                        key={item.id}
                        id={item.id}
                        title={item.title}
                        status={item.status}
                        toggleItemCompleted={toggleItemCompleted}
                    />
                    )}
                </table>
            </div>
        </div>
    );
}

export default App;
